<?php

/**
 * https://gist.github.com/cebe/5674918
 */
 
return [
  [
    'class' => 'yii\rest\UrlRule',
    'pluralize' => false,
    'only' => ['index'],
    'controller' => [
      'api/cites' => 'api/v1/city',
    ],
  ],
  [
    'class' => 'yii\rest\UrlRule',
    'pluralize' => false,
    'only' => ['cites'],
    'patterns' => [
      'GET,HEAD {id}' => 'cites',
    ],
    'controller' => [
      'api/streets' => 'api/v1/street',
    ],
  ],
  [
    'class' => 'yii\rest\UrlRule',
    'pluralize' => false,
    'controller' => [
      'api/contact' => 'api/v1/contact',
    ],
  ],
];