<?php

namespace app\models\api;

use Yii;

/**
 * This is the model class for table "{{%phones}}".
 *
 * @property integer $id
 * @property string $phone
 * @property integer $contact_id
 *
 * @property Contacts $contact
 */
class Phones extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%phones}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['phone', 'contact_id'], 'required'],
            [['contact_id'], 'integer'],
            [['phone'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'phone' => 'Номер телефона',
            'contact_id' => 'Contact ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContact()
    {
        return $this->hasOne(Contacts::className(), ['id' => 'contact_id']);
    }
}
