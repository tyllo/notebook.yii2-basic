<?php

namespace app\models\api;

use Yii;

/**
 * This is the model class for table "{{%contacts}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $surname
 * @property string $patronymic
 * @property string $avatar
 * @property string $bith_time
 * @property integer $street_id
 *
 * @property Streets $street
 * @property Phones[] $phones
 */
class Contacts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%contacts}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['avatar'], 'required'],
            [['bith_time'], 'safe'],
            [['street_id'], 'integer'],
            [['name', 'surname', 'patronymic', 'avatar'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'surname' => 'Фамилия',
            'patronymic' => 'Отчество',
            'avatar' => 'Аватар',
            'bith_time' => 'День рождения',
            'street_id' => 'Street ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStreet()
    {
        return $this->hasOne(Streets::className(), ['id' => 'street_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhones()
    {
        return $this->hasMany(Phones::className(), ['contact_id' => 'id']);
    }
}
