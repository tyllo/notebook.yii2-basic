<?php

use yii\db\Schema;
use yii\db\Migration;

class m150910_082252_initial_tables extends Migration
{
  private $_builder;
  private $_tableOption = 'ENGINE=InnoDB DEFAULT CHARSET=utf8';
  private $_tables = [
    'cites',
    'contacts',
    'phones',
    'streets',
  ];

  public function up()
  {
      // создадим таблицы и их данные по умолчанию
      foreach ($this->_tables as $table) {
        call_user_func(array($this, "create_$table"), $table);
      };

      $this->addForeignKey('fk_contacts_streets', '{{%contacts}}', ['street_id'], '{{%streets}}', ['id'], 'NO ACTION', 'NO ACTION');
      $this->addForeignKey('fk_phones_contacts', '{{%phones}}', ['contact_id'], '{{%contacts}}', ['id'], 'NO ACTION', 'NO ACTION');
      $this->addForeignKey('fk_streets_cites', '{{%streets}}', ['city_id'], '{{%cites}}', ['id'], 'NO ACTION', 'NO ACTION');
  }

  public function down()
  {
      $this->dropForeignKey('fk_contacts_streets', '{{%contacts}}');
      $this->dropForeignKey('fk_phones_contacts', '{{%phones}}');
      $this->dropForeignKey('fk_streets_cites', '{{%streets}}');

      // удалим таблицы и их данные по умолчанию
      foreach ($this->_tables as $table) {
        $this->dropTable('{{%' . $table . '}}');;
      };
  }

  private function create_cites($tablename)
  {
      $this->createTable('{{%' . $tablename . '}}', [
          'id' => $this->primaryKey(),
          'name' => $this->string()->notNull() . " COMMENT 'Город'",
      ], $this->_tableOption);

      $this->createIndex('idx_0', '{{%' . $tablename . '}}', 'id', FALSE);
  }

  private function create_contacts($tablename)
  {
      $this->createTable('{{%' . $tablename . '}}', [
          'id' => $this->primaryKey(),
          'name' => $this->string() . " COMMENT 'Имя'",
          'surname' => $this->string() . " COMMENT 'Фамилия'",
          'patronymic' => $this->string() . " COMMENT 'Отчество'",
          'avatar' => $this->string()->notNull() . " COMMENT 'Аватар'",
          'bith_time' => $this->time() . " COMMENT 'День рождения'",
          'street_id' => $this->integer(),
      ], $this->_tableOption);

      $this->createIndex('idx_0', '{{%' . $tablename . '}}', 'id', FALSE);
  }

  private function create_phones($tablename)
  {
      $this->createTable('{{%' . $tablename . '}}', [
          'id' => $this->primaryKey(),
          'phone'=>$this->string()->notNull() . " COMMENT 'Номер телефона'",
          'contact_id'=>$this->integer()->notNull(),
      ], $this->_tableOption);

      $this->createIndex('idx_0', '{{%' . $tablename . '}}', 'id', FALSE);
  }

  private function create_streets($tablename)
  {
      $this->createTable('{{%' . $tablename . '}}', [
          'id' => $this->primaryKey(),
          'name' => $this->string()->notNull() . " COMMENT 'Улица'",
          'city_id' => $this->integer()->notNull() . " COMMENT 'Город'",
      ], $this->_tableOption);

      $this->createIndex('idx_0', '{{%' . $tablename . '}}', 'id', FALSE);
  }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
