<?php

use yii\db\Schema;
use yii\db\Migration;

class m150924_131717_create_user_table extends Migration
{
    private $_tableOption = 'ENGINE=InnoDB DEFAULT CHARSET=utf8';
    
    const TABLE_NAME = 'user';

    public function up()
    {
        $this->createTable('{{%' . self::TABLE_NAME . '}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string() . " COMMENT 'Имя'",
            'surname' => $this->string() . " COMMENT 'Фамилия'",
            'patronymic' => $this->string() . " COMMENT 'Отчество'",
            'avatar' => $this->string()->notNull() . " COMMENT 'Аватар'",
            'time_bith' => $this->time() . " COMMENT 'День рождения'",
            'street_id' => $this->integer(),
        ], $this->_tableOption);

        $this->createIndex('idx_0', '{{%' . self::TABLE_NAME . '}}', 'id', FALSE);
    }

    public function down()
    {
        $this->dropTable('{{%' . self::TABLE_NAME . '}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
