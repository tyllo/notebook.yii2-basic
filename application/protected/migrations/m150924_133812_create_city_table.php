<?php

use yii\db\Schema;
use yii\db\Migration;

class m150924_133812_create_city_table extends Migration
{
    private $_tableOption = 'ENGINE=InnoDB DEFAULT CHARSET=utf8';

    const TABLE_NAME = 'city';
    const FOREIGN_TABLE_NAME = 'street';

    public function up()
    {
        $this->createTable('{{%' . self::TABLE_NAME . '}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull() . " COMMENT 'Город'",
        ], $this->_tableOption);

        $this->createIndex('idx_0', '{{%' . self::TABLE_NAME . '}}', 'id', FALSE);
        
        $this->addForeignKey(
          'fk_' . self::FOREIGN_TABLE_NAME . '_' . self::TABLE_NAME,
          '{{%' . self::FOREIGN_TABLE_NAME . '}}', ['city_id'],
          '{{%' . self::TABLE_NAME . '}}', ['id'],
          'NO ACTION', 'NO ACTION'
        );
    }

    public function down()
    {
        $this->dropForeignKey(
          'fk_' . self::FOREIGN_TABLE_NAME . '_' . self::TABLE_NAME,
          '{{%' . self::FOREIGN_TABLE_NAME . '}}'
        );

        $this->dropTable('{{%' . self::TABLE_NAME . '}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
