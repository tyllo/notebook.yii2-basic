<?php

use yii\db\Schema;
use yii\db\Migration;

class m150924_132439_create_user_phone_table extends Migration
{
    private $_tableOption = 'ENGINE=InnoDB DEFAULT CHARSET=utf8';
    
    const TABLE_NAME = 'user_phone';
    const FOREIGN_TABLE_NAME = 'user';

    public function up()
    {
        $this->createTable('{{%' . self::TABLE_NAME . '}}', [
            'id' => $this->primaryKey(),
            'phone'=>$this->string()->notNull() . " COMMENT 'Номер телефона'",
            'user_id'=>$this->integer()->notNull(),
        ], $this->_tableOption);

        $this->createIndex('idx_0', '{{%' . self::TABLE_NAME . '}}', 'id', FALSE);

        $this->addForeignKey(
            'fk_' . self::FOREIGN_TABLE_NAME . '_' . self::TABLE_NAME,
            '{{%' . self::TABLE_NAME . '}}', ['user_id'],
            '{{%' . self::FOREIGN_TABLE_NAME . '}}', ['id'],
            'NO ACTION', 'NO ACTION'
        );
    }

    public function down()
    {
        $this->dropForeignKey(
          'fk_' . self::FOREIGN_TABLE_NAME . '_' . self::TABLE_NAME,
          '{{%' . self::TABLE_NAME . '}}'
        );
        
        $this->dropTable('{{%' . self::TABLE_NAME . '}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
