<?php

// https://github.com/yiisoft/yii2/issues/6877

namespace app\controllers\api\v1;

use Yii;
use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;

class BaseController extends ActiveController
{
    public function behaviors()
    {
      return array_merge([
        'rateLimiter' => ['enableRateLimitHeaders' => false],
        'corsFilter' => [
          'class' => \yii\filters\Cors::className(),
            'cors' => [
              'Origin' => ['*'],
              'Access-Control-Request-Headers' => ['X-Wsse'],
              'Access-Control-Allow-Credentials' => true,
              'Access-Control-Max-Age' => 3600,
              'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
              'Access-Control-Request-Method' => [
                'GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'
              ],
            ],
          ]
        ], parent::behaviors());
    }

    public function actions(){
      $actions = parent::actions();
      unset($actions['index']);
      return $actions;
    }

    public function actionIndex(){

      $modelClass = $this->modelClass;

      return new ActiveDataProvider([
        'query' => $modelClass::find(),
          'pagination' => false,
      ]);
    }
}
