<?php

namespace app\controllers\api\v1;

use Yii;
use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;
use app\controllers\api\v1\Base;

class CityController extends BaseController
{
    public $modelClass = '\app\models\api\Cites';
}
