<?php

namespace app\controllers\api\v1;

use Yii;
use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use app\controllers\api\v1\Base;

class StreetController extends BaseController
{
    public $modelClass = '\app\models\api\Streets';

    public function actionCites($id){

      $modelClass = $this->modelClass;

      return new ActiveDataProvider([
        'query' => $modelClass::find()
          ->where(['city_id' => $id]),
          'pagination' => false,
      ]);
    }
}
