<?php

namespace app\components;

use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;

class Grabber extends Component

//class Grabber
{
  private static $_link = 'gorodskaya-spravka.com';
  private static $_cites;

  public function getCites()
  {
    
    if (self::$_cites) return self::$_cites;

    $cites = [];
    echo "  >> get http://" . self::$_link . "\n";
    $html = file_get_contents("http://" . self::$_link);
    $html = iconv('windows-1251', 'utf-8', $html);

    $pattern = '|var arr_url = \[(.*?)\]|i';
    $cites['link'] = self::_getCitesList($html, $pattern);

    $pattern = '|var arr_name = \[(.*?)\]|i';
    $cites['name'] = self::_getCitesList($html, $pattern);

    self::$_cites = array_combine($cites['link'] , $cites['name']);
    
    echo "  >> finish get http://" . self::$_link . "\n";
    return self::$_cites;
  }
  private function _getCitesList($html, $pattern)
  {
    preg_match_all("$pattern", "$html", $matches);
    $matches = str_replace(["'"," "], ["",""], $matches[1][0]);
    return explode(',', $matches);
  }
  public function getStreets($linkToCity)
  {
      $link = "http://{$linkToCity}." . self::$_link;
      $pages = $this->_parsCountPages($linkToCity);

      // ################# get streets ####################
      $page = 1; $streets = [];
      while(true):
          if ($page > $pages) break;
          $parsLink = $link.'/street/'.$page.'.html';

          echo "[ $parsLink ].. ";
          $html = file_get_contents($parsLink);
          $html = iconv('windows-1251', 'utf-8', $html);

          //<li><a href="/street/s_37304.html" title="1 Мая улица" target="_blank">1 Мая улица</a></li>
          /*$pattern = '|<li><a.*?title="(.*?)".*?>.*?<\/a><\/li>|iu';*/
          $pattern = '|<li><a.*?title="(.*?)".*?>.*?<\/a><\/li>|i';

          echo " pars streets!\n";
          preg_match_all("$pattern", "$html", $matches);
          $streets = array_merge($streets, $matches[1]);

          // увеличим счетчик страницы
          $page++;
      endwhile;
      return $streets;
  }
  private function _parsCountPages($linkToCity)
  {
      $link = "http://{$linkToCity}." . self::$_link;

      // ############### get number pages ################
      echo "[ $link ].. ";
      $html = file_get_contents($link . '/street/1.html');
      echo "get page!.. ";
      echo "pars pages.. ";
      //<a href="/street/3.html">
      $pattern = '|\/street\/([0-9]+)\.html|i';
      preg_match_all("$pattern", "$html", $matches);

      $pages = $matches[1][0];
      foreach( $matches[1] as $num)
          $pages = ($pages > $num) ? $pages : $num;
      echo "pages = $pages\n";

      return $pages;
  }
  private function _createINIfile()
  {
    // в этом файле награбленные улицы и города
    $file = __DIR__ . "/city.ini";

    foreach ($this->_cites as $city => $name):
      $arr[$city] = $this->getStreets($city);
      echo "[ $city - ready!!!!!!!! ]\n\n";

      $str = "\n[$name]";
      foreach ($arr[$city] as $id => $street):
        $str = "$str\n$id = '$street'";
      endforeach;
      file_put_contents($file, "$str\n", FILE_APPEND);
    endforeach; 

    return $file;
  }
}
