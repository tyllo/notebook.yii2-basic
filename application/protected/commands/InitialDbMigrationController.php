<?php
/**
 * https://gist.github.com/bmarston/5541632
 */

namespace app\commands;

use yii\console\Controller;

/**
 * Эта команда делает структуру базы данных из существующей
 */
class InitialDbMigrationController extends Controller
{
    /**
     * Эта команда создает стуркуту базы данных $args
     */
    public function actionRun($args) {
        $schema = $args;
        $tables = \Yii::$app->db->schema->getTableSchemas();
        $addForeignKeys = '';
        $dropForeignKeys = '';
        $result = "public function up()\n{\n";
        foreach ($tables as $table) {
            $compositePrimaryKeyCols = [];
            // Create table
            $result .= '    $this->createTable(\'' . $table->name . '\', [' . "\n";
            foreach ($table->columns as $col) {
                $result .= '        \'' . $col->name . '\'=>\'' . $col->type . '\',' . "\n";
                if ($col->isPrimaryKey && !$col->autoIncrement) {
                    // Add column to composite primary key array
                    $compositePrimaryKeyCols[] = $col->name;
                }
            }
            $result .= '    ], \'\');' . "\n\n";var_dump($table); die();
            // Add foreign key(s) and create indexes
            foreach ($table->foreignKeys as $col => $fk) {
                // Foreign key naming convention: fk_table_foreignTable_col (max 64 characters)
                $fkName = substr('fk_' . $table->name . '_' . $fk[0] . '_' . $col, 0 , 64);
                $tableName = $fk[0]; unset($fk[0]);
                $addForeignKeys .= '    $this->addForeignKey(' . "'{$fkName}', '{$table->name}', ['" .
                implode("','" , array_keys($fk)) . "'], '{$tableName}', ['" .
                implode("','" , array_values($fk)) . "'], 'NO ACTION', 'NO ACTION');\n\n";
                $dropForeignKeys .= '    $this->dropForeignKey(' . "'{$fkName}', '{$table->name}');\n\n";
                // Index naming convention: idx_col
                $result .= '    $this->createIndex(\'idx_' . $col . "', '{$table->name}', '{$col}', FALSE);\n\n";
            }
            // Add composite primary key for join tables
            if ($compositePrimaryKeyCols) {
                $result .= '    $this->addPrimaryKey(\'pk_' . $table->name . "', '{$table->name}', '" . implode(',', $compositePrimaryKeyCols) . "');\n\n";
            }
        }
        $result .= $addForeignKeys; // This needs to come after all of the tables have been created.
        $result .= "}\n\n\n";
        $result .= "public function down()\n{\n";
        $result .= $dropForeignKeys; // This needs to come before the tables are dropped.
        foreach ($tables as $table) {
            $result .= '    $this->dropTable(\'' . $table->name . '\');' . "\n";
        }
        $result .= "}\n";
        echo $result;
    }
    public function actionGetColType($col) {
        if ($col->isPrimaryKey && $col->autoIncrement) {
            return "pk";
        }
        $result = $col->dbType;
        if (!$col->allowNull) {
            $result .= ' NOT NULL';
        }
        if ($col->defaultValue != null) {
            $result .= " DEFAULT '{$col->defaultValue}'";
        } elseif ($col->allowNull) {
            $result .= ' DEFAULT NULL';
        }
        return $result;
    }
}
