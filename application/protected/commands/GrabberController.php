<?php

namespace app\commands;

use yii\console\Controller;
use app\models\api\City;
use app\models\api\Street;
use app\components\Grabber;

/**
 * Grabber cites and streets
 */
class GrabberController extends Controller
{
    /**
     Grabber cites and streets
     */
    public function actionIndex()
    {
      $grabber = new Grabber();

      foreach($grabber->cites as $linkToCity => $name) {
        $city = new City();
        $city->name = $name;
        $city->save();
        echo "\n  >> get streets of {$linkToCity} [{$city->id}]\n";
        foreach($grabber->getStreets($linkToCity) as $name) {
          $street = new Street();
          $street->name = $name;
          $street->city_id = $city->id;
          $street->save();
        }
        echo "  >> end get streets of {$linkToCity}\n";
      }
    }
}
