import gulp from 'gulp'
import errorHandler from '../helpers/errorHandler'
import config from '../config/styles'

import sass from 'gulp-sass'
import sourcemaps from 'gulp-sourcemaps'
import autoprefixer from 'gulp-autoprefixer'
import minify from 'gulp-minify-css'
import gulpif from 'gulp-if'

gulp.task('styles', () =>
  gulp
    .src(config.src)
    .pipe(errorHandler())
    .pipe(sourcemaps.init())
    .pipe(sass(config.settings))
    .pipe(sourcemaps.write({includeContent: false}))
    .pipe(sourcemaps.init({loadMaps: true}))
    .pipe(autoprefixer(config.autoprefixer))
    .pipe(sourcemaps.write('.'))
    .pipe(gulpif(config.minify, minify()))
    .pipe(gulp.dest(config.dest))
)
