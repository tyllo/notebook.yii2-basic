import gulp from 'gulp'
import config from '../config/jade'
import errorHandler from '../helpers/errorHandler'

import jade from 'jade'
import gulpJade from 'gulp-jade'
import sass from'gulp-sass'

import gulpif from 'gulp-if'
import minify from 'gulp-minify-html'

// фильтр :sass для jade файлов
jade.filters.sass = (str) => 
  sass.compiler.renderSync({
    data: str,
    outputStyle: 'compressed'
  }).css.toString()

// фильтр :php для jade файлов
jade.filters.php = (str) => 
  '\n<?php\n' + str + '\n?>'

// фильтр :json для jade файлов
jade.filters.json = (str) => str

const YOUR_LOCALS = {}
gulp.task('jade', () => 
  gulp
    .src(config.src)
    .pipe(errorHandler())
    .pipe(gulpJade({
      locals: YOUR_LOCALS,
      jade: jade,
      pretty: true
    }))
    .pipe(gulpif(config.minify, minify(config.minifyOpt)))
    .pipe(gulp.dest(config.dest))
)
