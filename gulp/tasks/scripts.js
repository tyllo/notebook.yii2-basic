import gulp from 'gulp'
import errorHandler from '../helpers/errorHandler'
import config from '../config/scripts'

import sourcemaps from 'gulp-sourcemaps'
import minify from 'gulp-uglify'
import babel from 'gulp-babel'
import webpack from 'webpack'
import gulpWebpack from 'gulp-webpack'
import concat from 'gulp-concat'
import gulpif from 'gulp-if'

gulp.task('scripts', () =>
  gulp
    .src(config.src)
    .pipe(errorHandler())
    .pipe(sourcemaps.init())
    .pipe(babel({
      optional: ['runtime'],
    }))
    // .pipe(gulp.dest(config.src))
    // .pipe( gulpWebpack(configs.webpack, webpack) )
    .pipe(gulpif(config.minify, minify()))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(config.dest))
)
