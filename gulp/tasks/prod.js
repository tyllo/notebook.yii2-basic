import gulp from 'gulp'
import errorHandler from '../helpers/errorHandler'
import config from '../config/prod'

gulp.task('prod', () => {
  gulp.start('prod:copy')
  gulp.start('prod:view')
});


gulp.task('prod:copy', () =>
  gulp
    .src(config.assets.src)
    .pipe(errorHandler())
    .pipe(gulp.dest(config.assets.dest))
);


gulp.task('prod:view', () =>
  gulp
    .src(config.views.src)
    .pipe(errorHandler())
    .pipe(gulp.dest(config.views.dest))
)
