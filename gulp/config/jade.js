import paths from '../paths'

export default {
  src: [
    '!' + paths.src + '/templates/**/*.jade',
    paths.src + '/**/*.jade',
  ],
  dest:  paths.dest,
  watch:  [
    paths.src + '/**/*.jade',
  ],
  uglify: paths.uglify.jade,
  minifyOpt: {
    conditionals: true,
    //spare:true
  }
}
