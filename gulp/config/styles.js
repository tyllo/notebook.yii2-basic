import paths from '../paths'

let dir = '/scss'

export default {
  src:   paths.src + dir + '/*.+(scss|sass)',
  dest:  paths.assets + paths.styles,
  watch: paths.src + dir + '/*.+(scss|sass)',
  settings: {
    includePaths: [
      './vendor/bower/foundation/scss',
      './vendor/bower/datetimepicker',
      './vendor/bower/font-awesome/scss',
      './src/scss'
    ],
    errLogToConsole: true,
  },
  autoprefixer: {
    browsers: ['last 5 versions']
  },
  uglify: paths.uglify.styles
}
