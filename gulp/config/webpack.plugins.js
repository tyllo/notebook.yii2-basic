import webpack from 'webpack'
import BowerWebpackPlugin from 'bower-webpack-plugin'
import ExtractTextPlugin from 'extract-text-webpack-plugin'

export default [
  // extract inline css into separate 'styles.css'
  new ExtractTextPlugin('styles.css'),
  // new webpack.optimize.UglifyJsPlugin(),
  new webpack.optimize.DedupePlugin(),
  //new webpack.DefinePlugin({
  //  'NODE_ENV': JSON.stringify('production')
  //}),
  new BowerWebpackPlugin({
      modulesDirectories: ['vendor/bower'],
      manifestFiles: ['bower.json', '.bower.json'],
      includes: /.*/,
      excludes: /.*\.(less|map)$/
   }),
]
