import ExtractTextPlugin from 'extract-text-webpack-plugin'

let loaders = {}

loaders.vue = { test: /\.vue$/, loader: 'vue' }

loaders.jade = { test: /\.jade/, loader: 'template-html-loader' }

loaders.js = {
  test: /\.js$/,
  exclude: /node_modules/,
  // loader: 'babel-loader' // <- without es6 polyfills
  loader: 'babel-loader?optional=runtime' // <- contain es6 polyfills
}

loaders.sass = {
  test: /\.(scss|sass)$/,
  loader: ExtractTextPlugin.extract({
    // activate source maps via loader query
    //'css?sourceMap!' + 'sass?sourceMap'
    loader: 'style!autoprefixer-loader?browsers=last 5 version!css!sass?indentedSyntax'
  })
}

loaders.css = {
  test: /\.css$/,
  loader: ExtractTextPlugin.extract('style-loader', 'css-loader!autoprefixer-loader?browsers=last 5 version')
}

loaders.image = {
  test: /.*\.(gif|png|jpe?g|svg)$/i,
  loaders: [
    'file?hash=sha512&digest=hex&name=[hash].[ext]',
    'image-webpack?{progressive:true,' +
    'optimizationLevel: 7,' +
    'interlaced: false,' +
    'pngquant:{quality: "65-90", speed: 4}}'
  ]
}

loaders.url = {
  test: /\.(eot|woff|ttf|svg|png|jpg)$/,
  loader: 'url-loader?limit=30000&name=[name]-[hash].[ext]'
}
export default [
  loaders.vue,
  loaders.jade,
  loaders.js,
  loaders.sass,
  loaders.css,
  loaders.image,
  loaders.url,
]
