import paths from '../paths'

export default {
  scripts: {
    src: [
      './vendor/bower/foundation/js/foundation.min.js',
      './vendor/bower/foundation/js/vendor/modernizr.js',
      './vendor/bower/jquery/dist/jquery.min.js',
      './vendor/bower/datetimepicker/jquery.datetimepicker.js',
      './vendor/bower/jquery-maskedinput/dist/jquery.maskedinput.min.js',
    ],
    dest: paths.assets + paths.scripts + '/vendor/',
    watch: [],
  },
  styles: {
    src: [],
    dest:  paths.assets + paths.styles + '/',
    watch: [],
  },
  fonts: {
    src: [
      './vendor/bower/font-awesome/fonts/**',
    ],
    dest:  paths.assets + paths.fonts + '/',
  },
  images: {
    src:   paths.src + paths.images + '/**/*.*',
    dest:  paths.assets + paths.images,
    watch: paths.src + paths.images + '/**/*',
  },
}
