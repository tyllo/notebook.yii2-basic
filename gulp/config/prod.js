import paths from '../paths'

let dir = '/scss'

export default {
  assets: {
    src: paths.assets + '/**/*.jade',
    dest: 'application/web/assets/',
  },
  views: {
    src: paths.src + '/templates/**/*',
    dest: 'application/protected/views/',
  },
}
