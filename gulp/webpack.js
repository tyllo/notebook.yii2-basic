import webpack from 'webpack'
import path from 'path'
import paths from './paths'

import plugins from './config/webpack.plugins'
import loaders from './config/webpack.loaders'

export default {
  context: path.join(__dirname, '../src'),
  entry: {
    app: './app.js',
  },
  output: {
    path: path.join(__dirname, '../dist/assets'),
    filename: '[name].js'
  },
  resolve: {
    extensions: ['', '.js'],
    modulesDirectories: ['node_modules'],
    alias: {
/*
      _svg:         path.join(_path, 'src', 'assets', 'svg'),
      _data:        path.join(_path, 'src', 'data'),
      _fonts:       path.join(_path, 'src', 'assets', 'fonts'),
      _modules:     path.join(_path, 'src', 'modules'),
      _images:      path.join(_path, 'src', 'assets', 'images'),
      _stylesheets: path.join(_path, 'src', 'assets', 'scss'),
      _templates:   path.join(_path, 'src', 'assets', 'templates')
*/
    },
  },
  module: {
    loaders: loaders,
  },
  // http://habrahabr.ru/post/245991/
  plugins: plugins,

  devtool: 'source-map'
}
