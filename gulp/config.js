import paths from './paths'
import webpack from './webpack'

import styles from './config/styles'
import scripts from './config/scripts'
import jade from './config/jade'
import copy from './config/copy'
import prod from './config/prod'
import browserSync from './config/browserSync'

export default {
  src:     paths.src,
  dest:    paths.dest,
  assets:  paths.assets,

  prod:    prod,
  copy:    copy,
  scripts: scripts,
  styles:  styles,
  jade:    jade,
  // webpack: webpack,
  browserSync: browserSync
}
