// app.js

import $ from 'jquery'
import foundation from 'foundation'

var app = {}

import InitPhone from 'modules/initPhone'
app.initPhone = InitPhone

import initDatepicker from 'modules/initDatepicker'
app.initDatepicker = initDatepicker

import restful from 'modules/restful'
app.restful = restful

app.initForm = function (form) {
  this.clone.form = $(form).children().clone(true, true)
}

start = function () {
  this.initPhone('.phone-container', '.phone-item')
  this.initDatepicker('.datetimepicker')
  this.initForm('form[name="create-user"]')
  return this
}

//////////////////////////////////////////////////////////
///

$(document).foundation()

app.start()

var testUrl = 'http://notebook.local/api'

app.restful = {
  url: testUrl,
  controller: 'cites',
  id: false,
}
