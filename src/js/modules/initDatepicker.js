// app/initDatepicker.js

import $ from 'jquery'
import datetimepicker from 'datetimepicker'

export default function initDatepicker (datepicker) {
  // http://xdsoft.net/jqplugins/datetimepicker/
  $(datepicker).datetimepicker({
    lang:'ru',
    timepicker:false,
    format:'Y-m-d',
    mask:true,
    closeOnDateSelect: 0,
    defaultDate:new Date(),
    dayOfWeekStart: 1,
  });
}
