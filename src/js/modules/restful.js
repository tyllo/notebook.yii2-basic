// app/Restful.js

import $ from 'jquery'

export default function restful (options) {

  options = $.extend({
    url: false,
    controller: false,
    id: false,
  }, options)

  var $container = $('select[name="' + options.controller + '"]')
  var $defaults = $container.find('option[value=""]')

  var create = function (data) {
    return data.reduce(function (result, current) {
      return result + '<option value="' + current.id + '">' + current.name + '</option>';
    }, '')
  }

  var success = function (data) {

    // установим значение начального option
    $defaults.text('...')

    // добавим в DOM элементы data
    $container.append(create( data ))

    $container.find('option[value="' + options.id + '"]')
    .attr("selected", "selected")

    // активируем поле option
    if(!options.id) {
      $container.removeAttr('disabled');
    }
  }

  var error = function (data) {

    $defaults.text('oops, error server')
    return $.Deferred();
  }

  var url = options.url + '/' + options.controller
  if (options.id !== false) url = url + '/' + options.id

  $.ajax({
    type: 'GET',
    url: url,
    dataType: "json",
  }).then(success, error)
}
