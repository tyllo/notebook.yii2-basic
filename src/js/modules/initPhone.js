// app/InitPhone.js

import $ from 'jquery'
import telHandler from '../plugins/jquery.telHandler'

export default function initPhone (container, item) {

  var $container = $(container);

  // format phone number
  $container
    .find('input[type="tel"]')
    .telHandler()

  // add new phone <input>
  // TODO: здесь определить модуль css с классом add
  // add extent classes: success, i.fa-plus
  $container.on('click', 'a.add', function (e) {

    e.preventDefault()

    // вставим клонированный phone в контейнер
    $clone.clone(true, true).appendTo(container);

    $(this)
      .removeClass('add')
      .addClass('remove')
  })

  // remove phone <input>
  // TODO: здесь определить модуль css с классом remove
  // remove extent classes: alert, i.fa-minus
  $container.on('click', 'a.remove', function (e) {
    e.preventDefault()

    // TODO: этот дестрой нужно обрабатывать
    // внутри плагина по событию remove
    $(this).closest(item)
      .find('input[type="tel"]')
      .telHandler('destroy')

    $(this)
      .closest(item)
      .remove()
  })

  var $clone = $(item).clone(true, true)
}
