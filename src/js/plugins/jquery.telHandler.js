// plugin jquery.telHandler

import jQuery from 'jquery'

export default (function telHandler ($) {

  var name = 'telHandler';

  var methods = {

    init: function () {

      if (this.data(name)) return this;

      this.data(name, {
        target: this,
      })

      return this.each(function () {
        return methods.replace.apply(this, arguments)
      })
    },

    destroy: function () {

      return this.each(function () {

        $(this).unbind('.' + name)
        return this
      })
    },

    replace: function (options) {

      var $this = $(this)

      options = $.extend({
        replace: /[^-0-9\s+()\.]/g,
      }, options)

      $this.bind('keyup.' + name, function () {

        var correct = this.value.replace(options.replace, '')

        if (this.value != correct) {
           this.value = correct
        }
      })
    }
  }

  $.fn.telHandler = function (method) {

    // логика вызова метода
    if ( methods[method] ) {
      return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
    } else if ( typeof method === 'object' || ! method ) {
      return methods.init.apply( this, arguments );
    } else {
      $.error( 'Метод с именем ' +  method + ' не существует для jQuery.telHandler' );
    }
  }
})(jQuery)
